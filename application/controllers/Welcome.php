<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('m_login');
 
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		//$where = array(
		//	'username' => $username,
		//	'password' => md5($password)
		//);
		$cek = $this->m_login->cek_login("user",$username,$password);
		//echo ($cek);
		//die();
		if($cek->num_rows() > 0){
			$data  = $cek->row_array();
			$id = $data['id'];
			$username = $data['username'];
			$role = $data['role'];
			$data_session = array(
				'id' => $id,
				'username' => $username,
				'role' => $role,
				'status' => "login"
				);
			$this->session->set_userdata($data_session);
			if($role=='admin'){
				redirect(base_url("panel/admin"));
			}else{
				redirect(base_url("auth/admin"));
			}
 
		}else{
			$this->session->set_flashdata('msg', 'Login Gagal, Username / Password Salah');
			redirect('auth/admin');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('panel/admin'));
	}
}
