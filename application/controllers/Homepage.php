<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_page');
	}
 

	public function index()
	{
		$this->load->view('homepage');
	}

	public function view($slug)
	{
		$this->load->view('page');
	}
}
