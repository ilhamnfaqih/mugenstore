<?php 
 
class Admin extends CI_Controller{
 
	function __construct(){
		parent::__construct();
		$this->load->model('m_login');
		$this->load->model('m_page');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("auth/admin"));
		}
	}
 
	function index(){
		if($this->session->userdata('role')=='admin'){
			$data['rows'] = $this->m_page->show_data();
			$this->load->view('v_admin',$data);
		}else{
			echo "Access Denied";
		}
	}

	function tambah_member(){
		if($this->session->userdata('role') == 'admin'){
			$this->load->view('v_tambah_member');
		}else{
			echo "Access Denied";
		}
	}

	function submit_tambah(){
		if($this->session->userdata('role') == 'admin'){
			$config['upload_path']   = FCPATH.'/uploads/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$this->load->library('upload',$config);
			$this->upload->do_upload('foto');

			$email = $this->input->post('email');
			$nama = $this->input->post('nama');
			$wa = $this->input->post('wa');
			$kota = $this->input->post('kota');
			$slug = $this->input->post('slug');
			$foto = $this->upload->data('file_name');
			
	
			$data = array(
				'email' => $email,
				'nama' => $nama,
				'wa' => $wa,
				'kota' => $kota,
				'slug' => $slug,
				'foto' => $foto
			);
			$this->m_page->input_data($data,'member');
			$this->session->set_flashdata('response',"Data Berhasil disimpan! Silahkan isi data selanjutnya atau kembali ke halaman Rekap Data. ");
			redirect('Admin/tambah_member');
		}else{
			echo "Access Denied";
		}
	}
	function hapus_member($id){
		$this->db->delete('member', array('id' => $id));
	}
}