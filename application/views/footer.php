
<!--Spacer-->
<p style="margin-top:50px;"> </p>
<!--End of Spacer-->

<div class="container-fluid" style="background-color:#9d2b16; height:120px; padding-top:20px; padding-bottom:20px; width:100%;">
  <div class="row justify-content-center">
      <p style="color:#fff;">Copyright &copy; <?php echo date('Y');?> @ilhamnfaqih </p>
  </div>
</div>

<!-- Bottom Navbar -->
<nav class="p-0 navbar navbar-dark navbar-expand fixed-bottom" style="background-color:#e06f1f;">
    <ul class="navbar-nav nav-justified w-100">
        <li class="nav-item active">
            <a href="#" class="nav-link text-center">
			<svg xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em" fill="currentColor" class="bi bi-house-fill" viewBox="0 0 16 16">
			<path fill-rule="evenodd" d="M8 3.293l6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
			<path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
			</svg>
                <span class="small d-block">Beranda</span>
            </a>
        </li>
		<li class="nav-item">
            <a href="#" class="nav-link text-center">
				<span class="badge badge-pill badge-danger" style="position:absolute; margin-left:17px; margin-top:-10px; font-size:1em;">12</span>
			<svg xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em" fill="currentColor" class="bi bi-basket2-fill" viewBox="0 0 16 16">
			<path d="M5.929 1.757a.5.5 0 1 0-.858-.514L2.217 6H.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h.623l1.844 6.456A.75.75 0 0 0 3.69 15h8.622a.75.75 0 0 0 .722-.544L14.877 8h.623a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1.717L10.93 1.243a.5.5 0 1 0-.858.514L12.617 6H3.383L5.93 1.757zM4 10a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm3 0a1 1 0 0 1 2 0v2a1 1 0 1 1-2 0v-2zm4-1a1 1 0 0 1 1 1v2a1 1 0 1 1-2 0v-2a1 1 0 0 1 1-1z"/>
			</svg>
                <span class="small d-block">Keranjang</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link text-center">
			<span class="badge badge-pill badge-danger" style="position:absolute; margin-left:17px; margin-top:-10px; font-size:1em;">354</span>
			<svg xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em" fill="currentColor" class="bi bi-bell-fill" viewBox="0 0 16 16">
			<path d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2zm.995-14.901a1 1 0 1 0-1.99 0A5.002 5.002 0 0 0 3 6c0 1.098-.5 6-2 7h14c-1.5-1-2-5.902-2-7 0-2.42-1.72-4.44-4.005-4.901z"/>
			</svg>
                <span class="small d-block">Notifikasi</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link text-center">
			<svg xmlns="http://www.w3.org/2000/svg" width="1.5em" height="1.5em" fill="currentColor" class="bi bi-person-lines-fill" viewBox="0 0 16 16">
			<path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z"/>
			</svg>
                <span class="small d-block">Saya</span>
            </a>
        </li>
    </ul>
</nav>

<!--===============================================================================================-->	
	<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>