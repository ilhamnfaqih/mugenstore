<?php
  $this->load->view('admin_head');
?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-0 text-gray-800">Tambah Member</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>panel/admin"><i class="fas fa-arrow-left text-white-50"></i> Kembali</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid col-md-6">
      <div class="card shadow mb-4">
	 		<div class="alert alert-success" role="alert">
				<p class=""><?php echo $this->session->flashdata('response'); ?></p>
            </div>
			<div class="card-body">
	
              <form action="<?php echo base_url();?>admin/submit_tambah" method="POST" enctype="multipart/form-data">
                <div>
                  <label>Email</label>
                  <input type="text" class="form-control" name="email" required>
                </div>
				<div>
                  <label>Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama" required>
                </div>
				<div>
                  <label>Kota</label>
                  <input type="text" class="form-control" name="kota" required>
                </div>
				<div>
                  <label>No. WhatsApp</label>
                  <input type="text" class="form-control" name="wa" required>
                </div>
				<div>
                  <label>SLUG</label>
                  <input type="text" class="form-control" name="slug" required>
                </div>
                <div>
                  <label>Foto</label>
                  <input type="file" class="form-control" name="foto" required>
                </div>
                <span><br></span>
                <div style="float:right">
                  <input type="submit" class="btn btn-success">
                </div>
              </form>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('admin_foot');
?>