<?php
  $this->load->view('admin_head');
?>
        <!-- Begin Page Content -->
		 <div class="container-fluid">
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Data Member</h1></div>
            <p><a class="d-none d-sm-inline-block btn btn-sm btn-primary" href="<?php echo base_url();?>admin/tambah_member"><i class="fas fa-plus text-white-50"></i> Tambah Member</a></p>
        </div>
        <!-- /.container-fluid -->
      <div class="container-fluid">
      <div class="card shadow mb-4">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>WhatsApp</th>
                      <th>Foto</th>
                      <th>Kota</th>
                      <th>Register Date</th>
                      <th>Slug</th>
                      <th>URL</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
				            	<th>Action</th>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>WhatsApp</th>
                      <th>Foto</th>
                      <th>Kota</th>
                      <th>Register Date</th>
                      <th>Slug</th>
                      <th>URL</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      <?php 
                        $i = 1;
                        foreach($rows as $row){
                      ?>
                        <tr id="<?php echo $row->id; ?>">
                          <td><!--a href="Admin/edit_member/<?php echo $row->id; ?>"><i class="fas fa-edit" style="color:orange;"></i></a--> 
                            <i class="fas fa-trash remove" style="color:red; cursor:pointer;"></i></td>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $row->nama; ?></td>
                          <td><?php echo $row->email; ?></td>
                          <td><?php echo $row->wa; ?></td>
                          <td><img src="<?php echo base_url().'uploads/'.$row->foto; ?>" width="80px" alt=" Tidak ada foto"></td>
                          <td><?php echo $row->kota; ?></td>
                          <td><?php echo $row->date; ?></td>
                          <td><?php echo $row->slug; ?></td>
                          <td><a href="https://bisnisviral.id/<?php echo $row->slug; ?>">https://bisnisviral.id/<?php echo $row->slug; ?></a></td>
						            </tr>
                      <?php
                        $i++;
                        }
                      ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

      </div>
      <!-- End of Main Content -->
<?php
  $this->load->view('admin_foot');
?>
<script type="text/javascript">
    $(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");


        if(confirm('Apakah Anda yakin ingin menghapus data ini ?'))
        {
            $.ajax({
               url: '/admin/hapus_member/'+id,
               type: 'DELETE',
               error: function() {
                  alert('Maaf data tidak bisa dihapus');
               },
               success: function(data) {
                    $("#"+id).remove();
                    alert("Data Berhasil dihapus!");  
               }
            });
        }
    });


</script>