<?php
	$this->load->view('header.php');
?>
<!--Spacer-->
<p style="margin-top:50px;"> </p>
<!--End of Spacer-->

<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      	<img class="d-block w-100" src="<?php echo base_url(); ?>assets/images/banner1.jpg" alt="slider">
    </div>
    <div class="carousel-item">
		<img class="d-block w-100" src="<?php echo base_url(); ?>assets/images/banner1.jpg" alt="slider">
    </div>
    <div class="carousel-item">
    	<img class="d-block w-100" src="<?php echo base_url(); ?>assets/images/banner1.jpg" alt="slider">
    </div>
  </div>
</div>
<!--Spacer-->
<p style="margin-top:50px;"> </p>
<!--End of Spacer-->

<!-- Kategori -->
<div class="container-fluid">
	<h4 style="color:#e06f1f;"> Kategori </h4>
	<div style="text-align:right;">
		<a href="#" style="position:end; color:#e06f1f;"> Lihat Semua >> </a>
	</div>
    <div class="row flex-row flex-nowrap overflow-auto">
		<div class="col-3 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card" style="text-align:center; border:0px;">
				<center><img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-width:50px;"></center>
				<p style="color:#e06f1f;">Fashion</p>
			</div>
		</a>
        </div>
		
		<div class="col-3 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card" style="text-align:center; border:0px;">
				<center><img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-width:50px;"></center>
				<p style="color:#e06f1f;">Fashion</p>
			</div>
		</a>
        </div>
		<div class="col-3 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card" style="text-align:center; border:0px;">
				<center><img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-width:50px;"></center>
				<p style="color:#e06f1f;">Fashion</p>
			</div>
		</a>
        </div>
		<div class="col-3 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card" style="text-align:center; border:0px;">
				<center><img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-width:50px;"></center>
				<p style="color:#e06f1f;">Fashion</p>
			</div>
		</a>
        </div>
		<div class="col-3 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card" style="text-align:center; border:0px;">
				<center><img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-width:50px;"></center>
				<p style="color:#e06f1f;">Fashion</p>
			</div>
		</a>
        </div>
		
    </div>
</div>

<!--Spacer-->
<p style="margin-top:50px;"> </p>
<!--End of Spacer-->

<!-- Hot Sale -->
<div class="container-fluid">
	<h4 style="color:#e06f1f;"> Hot Sale </h4>
	<div style="text-align:right;">
		<a href="#" style="position:end; color:#e06f1f;"> Lihat Lainnya >> </a>
	</div>
    <div class="row flex-row flex-nowrap overflow-auto">
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-height:300px;">
				<h5 style="color:#e06f1f;">Rp. 15.120.499,-</h5>
			</div>
		</a>
        </div>
        <div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-height:300px;">
				<h5 style="color:#e06f1f;">Rp. 15.120.499,-</h5>
			</div>
		</a>
        </div>
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-height:300px;">
				<h5 style="color:#e06f1f;">Rp. 15.120.499,-</h5>
			</div>
		</a>
        </div>
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-height:300px;">
				<h5 style="color:#e06f1f;">Rp. 15.120.499,-</h5>
			</div>
		</a>
        </div>
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/jam.jpg" style="max-height:300px;">
				<h5 style="color:#e06f1f;">Rp. 15.120.499,-</h5>
			</div>
		</a>
        </div>
		
    </div>
</div>

<!--Spacer-->
<p style="margin-top:50px;"> </p>
<!--End of Spacer-->
<!-- Toko Sekitar -->
<div class="container-fluid">
	<h4 style="color:#e06f1f;"> Toko di Sekitarmu </h4>
	<div style="text-align:right;">
		<a href="#" style="position:end; color:#e06f1f;"> Lihat Lainnya >> </a>
	</div>
    <div class="row flex-row flex-nowrap overflow-auto">
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/store.jpg" style="max-height:300px; padding:5px;">
				<h5 style="color:#e06f1f;">Toko Ajib</h5>
				<p> 
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
					<path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
					</svg>
				Kota Cimahi
				</p>
			</div>
		</a>
        </div>

		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/store.jpg" style="max-height:300px; padding:5px;">
				<h5 style="color:#e06f1f;">Toko Ajib</h5>
				<p> 
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
					<path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
					</svg>
				Kota Cimahi
				</p>
			</div>
		</a>
        </div>
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/store.jpg" style="max-height:300px; padding:5px;">
				<h5 style="color:#e06f1f;">Toko Ajib</h5>
				<p> 
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
					<path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
					</svg>
				Kota Cimahi
				</p>
			</div>
		</a>
        </div>
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/store.jpg" style="max-height:300px; padding:5px;">
				<h5 style="color:#e06f1f;">Toko Ajib</h5>
				<p> 
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
					<path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
					</svg>
				Kota Cimahi
				</p>
			</div>
		</a>
        </div>
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/store.jpg" style="max-height:300px; padding:5px;">
				<h5 style="color:#e06f1f;">Toko Ajib</h5>
				<p> 
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt-fill" viewBox="0 0 16 16">
					<path d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 1 0-6 3 3 0 0 1 0 6z"/>
					</svg>
				Kota Cimahi
				</p>
			</div>
		</a>
        </div>
    </div>
</div>

<!--Spacer-->
<p style="margin-top:50px;"> </p>
<!--End of Spacer-->
<!-- Artikel -->
<div class="container-fluid">
	<h4 style="color:#e06f1f;"> Artikel Terbaru </h4>
	<div style="text-align:right;">
		<a href="#" style="position:end; color:#e06f1f;"> Lihat Lainnya >> </a>
	</div>
    <div class="row flex-row flex-nowrap overflow-auto">
		<div class="col-7 col-lg-3 col-md-3 col-xl-3">
		<a href="">
            <div class="card card-block" style="text-align:center;">
				<img src="<?php echo base_url(); ?>assets/images/artikel.jpg" style="max-height:300px; padding:5px;">
				<h5 style="color:#e06f1f;">Fashion 2021 Kini Hadir den...</h5>
				<p> 
				Posted : 25-2-2020
				</p>
			</div>
		</a>
        </div>

    </div>
</div>
<?php
	$this->load->view('footer.php');
?>